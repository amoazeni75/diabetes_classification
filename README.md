<h3>Pima Indians Onset of Diabetes Dataset Classification</h3>
 
 <p>
 In this project, we are going to use the Pima Indians onset of diabetes dataset. This is a
standard machine learning dataset available for free download from the UCI Machine Learning
repository. It describes patient medical record data for Pima Indians and whether they had an
onset of diabetes within five years. It is a binary classification problem (onset of diabetes as 1
or not as 0). The input variables that describe each patient are numerical and have varying
scales. Below lists the eight attributes for the dataset:
 </p>
 
 <ul>
 <li>Number of times pregnant.</li>

 <li>Plasma glucose concentration a 2 hours in an oral glucose tolerance test.</li>

 <li>Diastolic blood pressure (mm Hg).</li>

 <li>Triceps skin fold thickness (mm).</li>

 <li>2-Hour serum insulin (mu U/ml).</li>

 <li>Body mass index.</li>

 <li>Diabetes pedigree function.</li>

 <li>Age (years). </li>

 <li>Class, onset of diabetes within five years. </li>
 
 </ul>